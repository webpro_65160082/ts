let ourTuple: readonly [number, boolean, string];
ourTuple = [5, false, "Coding God was here"];
// ourTuple.push("Something new and wrong"); //ถ้าไม่กำหนด readonly จะสามารถ pushได้
console.log(ourTuple);
