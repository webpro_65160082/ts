type carYear = number;
type carType = string;
type carModel = string;

type Car ={
    year: carYear,
    type: carType,
    model: carModel
}

const car1: Car ={
    year: 2001,
    type: "Nissan",
    model: "XXX"
}
console.log(car1);
